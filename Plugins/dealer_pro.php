<?php
/*
Plugin Name: Dealer Pro
Description: Dealer Pro
Version: 1.0.0
Plugin URI: https://www.fiverr.com/wp_right  
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/
add_action('show_user_profile', 'my_user_profile_edit_action');
add_action('edit_user_profile', 'my_user_profile_edit_action');
function my_user_profile_edit_action($user) {
  $user_address =  get_user_meta($user->ID, 'user_address');
  $user_company =  get_user_meta($user->ID, 'user_company');
  $user_vatnumber =  get_user_meta($user->ID, 'user_vatnumber');
  $user_mobile =  get_user_meta($user->ID, 'user_mobile');
?>
<h3>Dealer Details</h3>
<table class="form-table">
<tbody>
<tr class="user-description-wrap">
	<th><label for="user_address">Address</label></th>
	<td><input name="user_address" type="text" id="user_address" value="<?php echo $user_address[0]; ?>">
</tr>
<tr class="user-description-wrap">
	<th><label for="user_company">Company</label></th>
	<td><input name="user_company" type="text" id="user_company" value="<?php echo $user_company[0]; ?>">
</tr>

<tr class="user-profile-picture">
	<th><label for="user_vatnumber"> VAT number</label></th>
	<td>
		<input name="user_vatnumber" type="text" id="user_vatnumber" value="<?php echo $user_vatnumber[0]; ?>">
	</td>
</tr>
<tr class="user-profile-picture">
	<th><label for="user_mobile">Mobile</label></th>
	<td>
		<input name="user_mobile" type="text" id="user_mobile" value="<?php echo $user_mobile[0]; ?>">
	</td>
</tr>
</tbody>
</table>
<?php 
}
add_action('personal_options_update', 'my_user_profile_update_action');
add_action('edit_user_profile_update', 'my_user_profile_update_action');
function my_user_profile_update_action($user_id) {
	$user_mobile = $_POST['user_mobile'];
	$user_vatnumber = $_POST['user_vatnumber'];
	$user_address = $_POST['user_address'];
	$user_company = $_POST['user_company'];
	update_user_meta( $user_id, 'user_company', $user_company );
	update_user_meta( $user_id, 'user_mobile', $user_mobile );
	update_user_meta( $user_id, 'user_vatnumber', $user_vatnumber ); 
	update_user_meta($user_id, 'user_address',$user_address );
}
add_action('wp_enqueue_scripts', 'geo_my_wp_script_front_css');
add_action('wp_enqueue_scripts', 'geo_my_wp_script_front_js');
add_action('admin_enqueue_scripts', 'geo_my_wp_script_back_css');
add_action('init', 'wp_astro_init');
add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );
add_shortcode( 'pdf', 'pdfpage');
add_shortcode( 'mail_template', 'mail_template');
add_shortcode( 'mail_template_file_rec', 'mail_template_file_rec');
add_shortcode( 'show_admin_dashboard', 'wp_show_admin_dashboard');
add_filter('wp_mail_from_name', 'from_dealer_name');	
add_filter('wp_mail_from', 'from_dealer_from');
add_action( 'wp_ajax_yezter_request_sign',  'send_request_signin' );	
add_action( 'wp_ajax_nopriv_yezter_request_sign',  'send_request_signin' );
register_activation_hook( __FILE__,  'install' );

function install(){
	add_role( 'dealer', 'dealer', array( 'read' => true, 'level_0' => true ) );
}
function wp_show_admin_dashboard(){
global $current_user;

if( !empty($current_user->roles) ){
foreach ($current_user->roles as $key => $value) {
if( $value == 'administrator' ){
  
if(isset($_GET['user_id'])){
$user_id = $_GET['user_id'];
//$user_id = 3;
$args = array(
  'author'        =>  $user_id,
  'post_type' => 'file',  
  'orderby'       =>  'post_date',
  'order'         =>  'DESC',
  'posts_per_page' => -1 // no limit
);

$current_user_posts = get_posts( $args );
?>
<a href="<?php echo get_permalink(); ?>"><input type="button" value="Back" class="btn btn-primary"></a>
<div class="container22" style="margin: 0px"><div class="row main_parent">
<?php
		foreach($current_user_posts as $post){
			$manufacturer = get_post_meta( $post->ID, 'manufacturer', true );
			$model = get_post_meta( $post->ID, 'model', true );
			$fuel_type = get_post_meta( $post->ID, 'fuel_type', true );
			$variant = get_post_meta( $post->ID, 'variant', true );
			$stage = get_post_meta( $post->ID, 'stage', true );
			$vehicle_registration = get_post_meta( $post->ID, 'vehicle_registration', true );
			$transmission = get_post_meta( $post->ID, 'transmission', true );
			$mileage = get_post_meta( $post->ID, 'mileage', true );
			$tool_used = get_post_meta( $post->ID, 'tool_used', true );
			$vehicle_age = get_post_meta( $post->ID, 'vehicle_age', true );
			$free_options = get_post_meta( $post->ID, 'free_options', true );
			$paid_options = get_post_meta( $post->ID, 'paid_options', true );
			$dealer_file = get_post_meta( $post->ID, 'dealer_file', true );
			$comments = get_post_meta( $post->ID, 'comments', true );
			$currentdate = get_the_date( 'l F j, Y' );

			echo ' <div class="main_container">
			<div id="show_date">'.$currentdate.'</div> 
			<div class="show_data"><div class="manufacturer">'.$manufacturer.' >> '.$model.' >> '.$vehicle_age.' >> '.$transmission.'>>'.$stage.'</div>';
			echo '</div></div>';
			//if($dealer_file){
			
				if(isset($_POST['reupload_file_submit'])){
					$post_id = $_POST['post_id'];
					$manufacturer = get_post_meta($post_id,'manufacturer');
					$model = get_post_meta($post_id,'model');
					$fuel_type = get_post_meta($post_id,'fuel_type');
					$stage = get_post_meta($post_id,'stage');
					$void_check = $_POST['void_check'];
					$post_author_id = get_post_field( 'post_author', $post_id );
					$user_info = get_userdata($post_author_id);
					$user_email = $user_info->user_email;
					$username = $user_info->user_nicename;
					$user_mobile = get_user_meta($post_author_id,'user_mobile');
					$user_mobile = $user_mobile[0];
					send_message($user_mobile);				
					if($void_check == 'yes'){
						$void_reason = $_POST['void_reason'];
						update_post_meta($post_id,'void_reason',$void_reason);
						$reupload_file = '';
					}else{
						$reupload_file = $_POST['reupload_file'];				
						$reupload_file = rtrim($reupload_file,',');
						add_filter( 'wp_mail_content_type','wp_set_content_type' );
						$html_temp = mail_template();
						//#manufacture# >> #model# >> #fuel_type# >> #stage#
						$html_temp = str_replace( '#manufacture#' ,$manufacturer[0], $html_temp );
						$html_temp = str_replace( '#model#' ,$model[0], $html_temp );
						$html_temp = str_replace( '#fuel_type#' ,$fuel_type[0], $html_temp );
						$html_temp = str_replace( '#stage#' ,$stage[0], $html_temp );
						//$user_email = 'hassaniqbal.dev@gmail.com';
						wp_mail( $user_email, 'Remmapped File', $html_temp);
						remove_filter( 'wp_mail_content_type', 'wp_set_content_type' );
					}
					
					update_post_meta($post_id,'dealer_file',$reupload_file);
					

				}
				echo '<a href="'.$dealer_file.'" class="invoice_link btn btn-default d-right margin-clear" download>Download File</a><form action="" class="ci_form" id="form" method="post">';
				 $args = array(
					"unique_identifier" => "my_subscription_form_file_upload",
					"allowed_extensions" => "zip,pdf,rar",
					"on_success_set_input_value" => "#file_remap_upload",
					"disallow_remove_button" => "1",
					"on_success_alert" => "Your file has been uploaded. Please submit it."
				);
				echo ajax_file_upload( $args );
				echo '<textarea class="hide" name="reupload_file" id="file_remap_upload"></textarea>';
				echo '<input name="post_id" value="'.$post->ID.'" type="hidden">';
				echo '<div class="btn btn-submit void_check_btn"><label class="void_label"><span class="void_check_text">Void File</span><input id="void_check"  name="void_check" value="yes" type="checkbox"> <span class="checkmark"></span></label></div>';
				echo '<textarea id="show_void_text" name="void_reason" style="display:none;"></textarea>';
				echo '<input class="btn btn-submit reupload_file" name="reupload_file_submit" Value="Resubmit File" type="submit"></form>';
			//}
		}
	}else{
		$html_result = wp_show_dealer_list();
		echo $html_result;
	}
      
      }else{
		  echo '<h3>Only Admin can see this page.</h3>';
	  }
    }
  }
}

function wp_show_dealer_list(){

$blogusers = get_users( 'blog_id=1&orderby=nicename&role=dealer' );
// Array of WP_User objects.
$site_url = get_permalink();
ob_start();
	
	?><div class="files_list_main">
		<h2>Dealers List</h2>
		<?php
	$count_files_number = 1;
	foreach ( $blogusers as $user ) {
		echo '<div class="main_admin_files"><span class="files_count">'.$count_files_number.'</span><div class="user_nicname_show">' . esc_html( $user->user_nicename ) . '</div><div class="uploaded_files_btn btn btn-danger"><a href="'.$site_url.'/?user_id='.$user->ID.'">See uploaded files</a></div></div>';
	$count_files_number ++;
	}
	
	?></div><?php
return ob_get_clean();

}

function send_request_signin(){
	if( isset( $_GET['useremail'] ) ){
		$useremail = $_GET['useremail'];
		$password = $_GET['u_password'];
		$company = $_GET['company'];
		$mobile = $_GET['mobile'];
		$address = $_GET['address'];
		$vatnumber = $_GET['vatnumber'];
		
		$user_id = wp_create_user( $useremail, $password, $useremail );
		$my_user = new WP_User( $user_id );
		$my_user->set_role( "dealer" );
		
		update_user_meta( $user_id, 'user_company', $company );		
		update_user_meta( $user_id, 'user_mobile', $mobile );	
		update_user_meta( $user_id, 'user_address', $address );	
		update_user_meta( $user_id, 'user_vatnumber', $vatnumber );
		
		$creds = array();
					
		$creds['user_login'] = $useremail;
		$creds['user_password'] = $password;
		$creds['remember'] = true;
		$user = wp_signon( $creds, false );
		wp_redirect( home_url('dealer-registration') );
		exit;
	}else{
		wp_redirect( home_url( 'dealer-registration' ) );
		exit;
	}
}
function from_dealer_name(){
	$wpfrom = "DDRemapped.com";
	return $wpfrom;
}
		
function from_dealer_from(){
	$wpfrom = 'admin@DDRemapped.com';
	return $wpfrom;
}
function random($length = 8){      
	$chars = '123456789bcdfghjklmnprstvwxzaeiou@#$%^';
	for ($p = 0; $p < $length; $p++)
	{
		$result .= ($p%2) ? $chars[mt_rand(19, 23)] : $chars[mt_rand(0, 18)];
	}
	return $result;
}	
function wp_set_content_type(){
	return "text/html";
}
function confirm_user_mail(){
	ob_start();
	?>
	<div>
  <div>
    <div style="background:black;text-align:center;">
		<div class="" style="">
			<a href="<?php echo home_url(); ?>">
			<img style="width:150px" src="<?php echo plugins_url('images/logo-invoice.jpeg', __FILE__); ?>">
			</a>
		</div>
    </div>
    <div>
      <div style="background: #000;padding: 25px 0px;">
        <div class="" style="text-align: center;font-family: arial;">
          <h2 style="color: #337ab7;">Dear #username#</h2>
          <p style="color: #fff;">
		  Thank you for registering with us. You are now required to verify your email and activate your account.</p> <p style="color: #fff;">Please click on the following link to activate your account.</p> 
		  <p>
			<a  href="#yezter_link#" style="text-decoration:none;color:#fff;padding:5px 15px;background-color:#19558f;font-size:14px;border-radius:3px" target="_blank" >Click Here</a> to Verify
		  </p>
        </div>
      </div>
    </div>
	<div style="background: #666;margin-top: 0; padding: 15px 25px; font-weight: 600;color: #eee;">
		<div style="text-align:center;">
			<p>Best Regards,</p>
			<p><?php echo home_url(); ?> Team.</p>
		</div>
	</div>
  </div>
</div>
	<?php
	$html = ob_get_clean();
	return $html;
}
		
function mail_template(){
		ob_start();
	?>
<div>
  <div>
    <div style="background:black;text-align:center;">
		<div class="" style="">
			<a href="<?php echo home_url(); ?>">
			<img style="width:150px" src="<?php echo plugins_url('images/logo-invoice.jpeg', __FILE__); ?>">
			</a>
		</div>
    </div>
    <div>
      <div style="background: #000;padding: 25px 0px;">
        <div class="" style="text-align: center;font-family: arial;">
          <h2 style="color: #337ab7;">Remapped File</h2>
          <p style="color: #fff;">Your remapped file is ready for the followign vehicle:</p>
          <div style="font-weight: 600;color: #fff;">#manufacture# &gt;&gt; #model# &gt;&gt; #fuel_type# &gt;&gt; #stage#</div>
        </div>
        <div style="text-align:center;"><a href="<?php echo home_url(); ?>/dealer-regisdivation/" style="color: #337ab7;font-weight: bolder;"> &gt;&gt; Log into the dealer portal &lt;&lt; </a></div>
      </div>
    </div>
    <div style="background: #666;margin-top: 0; padding: 15px 25px; font-weight: 600;color: #eee;">
      <div class="" style="">DD Remapping &amp; Diagnostic Ldiv | Unit 6, Fell Road, Sheffield, S9 2AL</div>
      <div class="" style="">Tel: 07377126544 | Email: <span style="color: #337ab7;">info@ddremapping.com</span></div>
    </div>
  </div>
</div>
	<?php
	$html_pdf = ob_get_clean();
	return $html_pdf;
}
function mail_template_file_rec(){
		ob_start();
	?>
<div>
  <div>
    <div style="background:black;text-align:center;">
		<div class="" style="">
			<a href="<?php echo home_url(); ?>">
			<img style="width:150px" src="<?php echo plugins_url('images/logo-invoice.jpeg', __FILE__); ?>">
			</a>
		</div>
    </div>
    <div>
      <div style="background: #000;padding: 25px 0px;">
        <div class="" style="text-align: center;font-family: arial;">
          <h2 style="color: #337ab7;">File received</h2>
          <p style="color: #fff;padding: 0 55px;">Thank you for uploading your file, we can confirm that we have received it and it is surrently in the queue.</p>
          <div style="font-weight: 600;color: #fff;">To check the status of your file please go to the dealer portal.</div>
        </div>
        <div style="text-align:center;"><a href="<?php echo home_url(); ?>/dealer-registration/" style="color: #337ab7;font-weight: bolder;"> &gt;&gt; Log into the dealer portal &lt;&lt; </a></div>
      </div>
    </div>
    <div style="background: #666;margin-top: 0; padding: 15px 25px; font-weight: 600;color: #eee;">
      <div class="" style="">DD Remapping &amp; Diagnostic Ldiv | Unit 6, Fell Road, Sheffield, S9 2AL</div>
      <div class="" style="">Tel: 07377126544 | Email: <span style="color: #337ab7;">info@ddremapping.com</span></div>
    </div>
  </div>
</div>
	<?php
	$html_pdf = ob_get_clean();
	return $html_pdf;
}
function pdfpage(){
	ob_start();
	?>
<table>
<body>
<tr>
	<td class="" style="width: 350px;float: left;"><img style ="width:150px" src="<?php echo plugins_url('images/logo-invoice.jpeg', __FILE__); ?>"></td>
	<td>
		<div class="col-sm-4 control-label" style="text-align: right;font-family: arial;font-size: 14px;width: 350px;float: left;">
		 <div style="line-height: 16px;font-weight: 600;">Remaping and Diagnostic LTD</div>
		 <div style="line-height: 16px;font-weight: 600;">info@ddremapping.com</div>
		 <div style="line-height: 16px;font-weight: 600;">Tel: 07377126544</div>
		</div>
	</td>
</tr>

<tr>
<td>
<td class="" style="width: 100%;margin: 0% 0%;">
	<div class="" style="background-color: #000;color: #fff;padding: 5px 10px;">Invoice</div>
	<div class="" style="background-color: #ddd;color: #000;padding: 5px 10px;">#addressinvoice#</div>
</td>

<td>
<div class="" style="width: 100%;">
    <div class="" style="background-color: #fff;color: #fff;padding: 5px 10px;margin-top: 21px;"></div>
    <div class="" style="background-color: #ddd;color: #000;padding: 5px 10px;">
		<div>Invoice number:#invoicenumber#</div>
		<div>Invoice date:#invoicedate#</div>
		<div>Due Date:#invoicedate_order#</div>
    </div>
</div>
</td>
</tr>



<tr style="margin-bottom: 25px;">
<td colspan="2" style="width:100%;">	
	<div class="" style="margin-top: 35px;">
		<div class="col-sm-12" style="width: 100%%;margin: 0 1%;margin-top: 35px;">
		<div class="head" style="border: 1px solid #999;background: #dddddd;padding: 4px 8px;font-weight: bold;">Items:</div>
		<div class="items" style="border: 1px solid #999;padding: 5px;">#invoice_item# - &#163;#total_amount#</div>
		</div>
	</div>
</td>
</tr>

<tr>
	
<td>	
</td>
<td style="margin-left: 10px;">	
	<div class="" style="display:inline-block;padding-left: 15px;width: 48%;">
		<div style="border: 1px solid #999;padding: 5px 20px 5px 5px;font-weight: bold;">Subtotatal:</div>
		<div style="border: 1px solid #999;padding: 5px 20px 5px 5px;font-weight: bold;">20% VAT:</div>
		<div style="border: 1px solid #999;padding: 5px 20px 5px 5px;font-weight: bold;background: #ddd;">Total:</div>
	</div>

	<div class="" style="display:inline-block;float:right;width: 48%;">
		<div style="border: 1px solid #999;padding: 5px 20px 5px 5px;">&#163;#total_amount#</div>
		<div style="border: 1px solid #999;padding: 5px 20px 5px 5px;">&#163;#tax_amount#</div>
		<div style="border: 1px solid #999;padding: 5px 20px 5px 5px;background: #ddd;">&#163;#total_invoice_amount#</div>
	</div>

</td>
</tr>

<tr>
<td colspan="2">
	<div class="" style="text-align: center;margin-top: 20px;width: 100%;">
		<p>DD Remapping &amp; 17 Willowbridge Close, Retford, Nottinghamshire , Dn22 6pg ,United Kingdom</p>
		<!--<p>Registered company No: 06724570 | VAT No : 264980273</p>-->
	</div>
</td>
</tr>

</body>

</table>

	<?php
	$html_pdf = ob_get_clean();
	return $html_pdf;
}
function wpdocs_register_my_custom_menu_page(){

  /*  add_menu_page( 

        __( 'Dog Breed Setting', 'textdomain' ),

        'Price Setting',

        'manage_options',

        'price_setting',

        'price_setting_menu_page'

    );  */
	
	 add_menu_page( 

        __( 'Stripe Setting', 'textdomain' ),

        'Stripe Gateway Setting',

        'manage_options',

        'stripe_setting',

        'stripe_setting_menu_page'

    ); 

}
function stripe_setting_menu_page(){

	if(isset($_POST['submit_price'])){
	 error_reporting(E_ALL);
	ini_set('display_errors', 1);
		$file_price = $_POST['file_price']; 
		$stripeKey = $_POST['_stripe_key'];
		$stripeSecret_Key = $_POST['_secret_key'];

		update_option('file_price',$file_price);
		
		update_option( 'dealer_stripe_key', $stripeKey );
		update_option( 'dealer_secret_key', $stripeSecret_Key );
		$user_mobile = '+447377126544';
		send_message($user_mobile);

	}

	$file_price = get_option('file_price');
	
	$stripe_key = get_option( 'dealer_stripe_key' );
	$secret_key = get_option( 'dealer_secret_key' );



   ?>

<form action="" class="ci_form" id="form" method="post">

<h2>Stripe Setting</h2>

<div class="">
<label>Stripe Key</label>
<input type="text" class="form-control dog_name_1" placeholder="_stripe_key" value="<?php echo $stripe_key ?>" required="required" name="_stripe_key">
</div>
<div class="">
<label>Secret Key</label>
<input type="text" class="form-control dog_name_1" placeholder="Secret Key" value="<?php echo $secret_key ?>" required="required" name="_secret_key">
</div>
<div class="">
<label>Default Price</label>
<input type="text" class="form-control dog_name_1" placeholder="Price" value="<?php echo $file_price ?>" required="required" name="file_price">
</div>
<div class="">
<!--<input type="submit" name="submit_price" id="feed_setting" value="Submit"> -->
<input type="submit" name="send_sms_message" id="feed_setting" value="Submit">
</div>


</form>

   <?php

}
function wp_astro_init() {
require_once(ABSPATH.'/wp-content/plugins/dealer_pro/sendex/sendex.php');	
   add_shortcode('show_dealer_registration', 'wp_geo_my_wp_form');
  $labels = array(
		'name'               => __( 'Files' ),
		'singular_name'      => __( 'File' ),
		'add_new'            => __( 'Add New File' ),
		'add_new_item'       => __( 'Add New File' ),
		'edit_item'          => __( 'Edit File' ),
		'new_item'           => __( 'Add New File' ),
		'view_item'          => __( 'View File' ),
		'search_items'       => __( 'Search File' ),
		'not_found'          => __( 'No file found' ),
		'not_found_in_trash' => __( 'No file found in trash' )
	);
	$supports = array(
		'title',
		'editor',
		'thumbnail',
		'comments',
		'revisions',
	);
	$args = array(
		'labels'               => $labels,
		'supports'             => $supports,
		'public'               => true,
		'capability_type'      => 'post',
		'rewrite'              => array( 'slug' => 'file' ),
		'has_archive'          => true,
		'menu_position'        => 30,
		'menu_icon'            => 'dashicons-calendar-alt',
		'register_meta_box_cb' => 'add_events_metaboxes_v2',
	);
	register_post_type( 'file', $args );
}

/**
 * If you wanted to have two sets of metaboxes.
 */
function add_events_metaboxes_v2() {
	add_meta_box(
		'wpt_events_location',
		'File Date',
		'wpt_events_location',
		'file',
		'normal',
		'high'
	);
}
/**
 * Output the HTML for the metabox.
 */
function wpt_events_location() {
	global $post;
	// Nonce field to validate form request came from current site
	wp_nonce_field( basename( __FILE__ ), 'event_fields' );
	// Get the location data if it's already been entered
	$fuel_type = get_post_meta( $post->ID, 'fuel_type', true );
	$manufacturer = get_post_meta( $post->ID, 'manufacturer', true );
	$model = get_post_meta( $post->ID, 'model', true );
	$fuel_type = get_post_meta( $post->ID, 'fuel_type', true );
	$variant = get_post_meta( $post->ID, 'variant', true );
	$stage = get_post_meta( $post->ID, 'stage', true );
	$vehicle_registration = get_post_meta( $post->ID, 'vehicle_registration', true );
	$transmission = get_post_meta( $post->ID, 'transmission', true );
	$mileage = get_post_meta( $post->ID, 'mileage', true );
	$tool_used = get_post_meta( $post->ID, 'tool_used', true );
	$vehicle_age = get_post_meta( $post->ID, 'vehicle_age', true );
	$free_options = get_post_meta( $post->ID, 'free_options', true );
	$paid_options = get_post_meta( $post->ID, 'paid_options', true );
	$invoice_link = get_post_meta( $post->ID, 'invoice_link', true );
	$invoice_price = get_post_meta( $post->ID, 'invoice_price', true );
	$dealer_file = get_post_meta( $post->ID, 'dealer_file', true );
	$void_reason = get_post_meta( $post->ID, 'void_reason', true );
	$comments = get_post_meta( $post->ID, 'comments', true );
	// Output the field
	echo '<h3><b class="col-sm-3 control-label">Vehicle Details</b></h3>';
	
	echo '<p><b class="col-sm-3 control-label">Manufacturer<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="manufacturer" value="' . esc_textarea( $manufacturer )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Model<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="model" value="' . esc_textarea( $model )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Fuel type<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="fuel_type" value="' . esc_textarea( $fuel_type )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Variant<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="variant" value="' . esc_textarea( $variant )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Stage<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="stage" value="' . esc_textarea( $stage )  . '" class="widefat">';
	
	echo '<h3 class="col-sm-9 col-sm-offset-3">More details</h3>';
	
	echo '<p><b class="col-sm-3 control-label">Vehicle registration<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="vehicle_registration" value="' . esc_textarea( $vehicle_registration )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Transmission<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="transmission" value="' . esc_textarea( $transmission )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Mileage<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="mileage" value="' . esc_textarea( $mileage )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Vehicle age<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="vehicle_age" value="' . esc_textarea( $vehicle_age )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Tool used<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="tool_used" value="' . esc_textarea( $tool_used )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Free options<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="free_options" value="' . esc_textarea( $free_options )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Paid options<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="paid_options" value="' . esc_textarea( $paid_options )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Dealer File<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="dealer_file" value="' . esc_textarea( $dealer_file )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Void Reason<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="void_reason" value="' . esc_textarea( $void_reason )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Invoice Link<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="invoice_link" value="' . esc_textarea( $invoice_link )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Invoice price<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="invoice_price" value="' . esc_textarea( $invoice_price )  . '" class="widefat">';
	echo '<p><b class="col-sm-3 control-label">Comments<span class="requiredinput">:*</span></b></p>';
	echo '<input type="text" name="comments" value="' . esc_textarea( $comments )  . '" class="widefat">';
}
/**
 * Save the metabox data
 */
function wpt_save_events_meta( $post_id, $post ) {
	// Return if the user doesn't have edit permissions.
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}
	// Verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times.
	if ( ! isset( $_POST['manufacturer'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	// Now that we're authenticated, time to save the data.
	// This sanitizes the data from the field and saves it into an array $events_meta.
	$events_meta['manufacturer'] = esc_textarea( $_POST['manufacturer'] );
	$events_meta['fuel_type'] = esc_textarea( $_POST['fuel_type'] );
	$events_meta['model'] = esc_textarea( $_POST['model'] );
	$events_meta['variant'] = esc_textarea( $_POST['variant'] );
	$events_meta['stage'] = esc_textarea( $_POST['stage'] );
	$events_meta['vehicle_registration'] = esc_textarea( $_POST['vehicle_registration'] );
	$events_meta['transmission'] = esc_textarea( $_POST['transmission'] );
	$events_meta['mileage'] = esc_textarea( $_POST['mileage'] );
	$events_meta['vehicle_age'] = esc_textarea( $_POST['vehicle_age'] );
	$events_meta['tool_used'] = esc_textarea( $_POST['tool_used'] );
	$events_meta['fuel_type'] = esc_textarea( $_POST['fuel_type'] );
	$events_meta['free_options'] = esc_textarea( $_POST['free_options'] );
	$events_meta['paid_options'] = esc_textarea( $_POST['paid_options'] );
	$events_meta['invoice_link'] = esc_textarea( $_POST['invoice_link'] );
	$events_meta['invoice_price'] = esc_textarea( $_POST['invoice_price'] );
	$events_meta['dealer_file'] = esc_textarea( $_POST['dealer_file'] );
	$events_meta['void_reason'] = esc_textarea( $_POST['void_reason'] );
	$events_meta['comments'] = esc_textarea( $_POST['comments'] );
	// Cycle through the $events_meta array.
	// Note, in this example we just have one item, but this is helpful if you have multiple.
	foreach ( $events_meta as $key => $value ) :
		// Don't store custom data twice
		if ( 'revision' === $post->post_type ) {
			return;
		}
		if ( get_post_meta( $post_id, $key, false ) ) {
			// If the custom field already has a value, update it.
			update_post_meta( $post_id, $key, $value );
		} else {
			// If the custom field doesn't have a value, add it.
			add_post_meta( $post_id, $key, $value);
		}
		if ( ! $value ) {
			// Delete the meta key if there's no value
			delete_post_meta( $post_id, $key );
		}
	endforeach;
}
add_action( 'save_post', 'wpt_save_events_meta', 1, 2 );
function wp_geo_my_wp_form($atts) {
	 error_reporting(E_ALL);
	ini_set('display_errors', 1);
	ob_start();
	
	if(isset($_POST['sign_forgot']) ){
		$forgot_email = $_POST['user_loginname'];
		
		$user = get_user_by( 'email', $forgot_email );
		$user_id = $user->ID;
		
		if($user_id){
			$password = random();
			
			wp_set_password( $password, $user_id );
			
			add_filter( 'wp_mail_content_type','wp_set_content_type' );
			
			wp_mail( $forgot_email, 'New Password', 'Your New Password is '.$password );
			
			remove_filter( 'wp_mail_content_type', 'wp_set_content_type' );
			
			echo "<h2>Please check your email for password retrieval</h2>";
		}else{
			echo "<h2>User with this email not exist</h2>";
		}
	}
	if(isset($_POST['sign_inuser']) ){
		$username = $_POST['user_loginname'];
		$password = $_POST['user_paswd'];
		
		$creds = array();
					
		$creds['user_login'] = $username;
		$creds['user_password'] = $password;
		$creds['remember'] = true;
		$user = wp_signon( $creds, false );
		if ( is_wp_error($user) ){
			echo '<h4 style="color:red">Incorrect user name or password</h4>';
		}else{
			wp_redirect( home_url('dealer-registration') );
		}
	}
	
	$user_id = get_current_user_id();
	
	if(isset($_POST['submit_user'])){
		//$method = $_POST['method'];
		$name = $_POST['user_name'];
		$company = $_POST['company'];
		$email = $_POST['email'];
		$mobile =  $_POST['mobile'];
		$address = $_POST['address'];
		$vatnumber = $_POST['vatnumber'];
		$password = $_POST['password'];
		$password2 = $_POST['password2']; 
		
		if ( username_exists( $email ) == true ) {
			echo '<h2>User name already exists.</h2>';
				
		}elseif (  email_exists($email) == false ) {
		/* $email_template_register = confirm_user_mail();
							
			$email_template_register = str_replace( '#username#' ,$name, $email_template_register );
			$email_template_register = str_replace( '#yezter_link#' , home_url().'/wp-admin/admin-ajax.php?action=yezter_request_sign&name='.$name.'&username='.$email.'&useremail='.$email.'&u_password='.$password.'&company='.$company.'&mobile='.$mobile.'&address='.$address.'&vatnumber='.$vatnumber, $email_template_register );
				
			add_filter( 'wp_mail_content_type','wp_set_content_type' );
			
			wp_mail( $email, 'Please verify your account', $email_template_register  );
			
			remove_filter( 'wp_mail_content_type', 'wp_set_content_type' );
			
			echo "<p style='font-size:16px;'>A confirmation email has been sent to the email address supplied. You are required to click on the link in this email to activate your account.</p>";
			 */
			 
			$user_id = wp_create_user( $email, $password, $email );
			$my_user = new WP_User( $user_id );
			$my_user->set_role( "dealer" );
			
			update_user_meta( $user_id, 'first_name', $name );		
			update_user_meta( $user_id, 'user_company', $company );		
			update_user_meta( $user_id, 'user_mobile', $mobile );	
			update_user_meta( $user_id, 'user_address', $address );	
			update_user_meta( $user_id, 'user_vatnumber', $vatnumber );
			
			echo "<h2>User Created Successfully. Login to continue</h2>";
		}
	} 

if ( is_user_logged_in() ) { 
$user = new WP_User( $user_id );
//delete_user_meta($user_id, '_stripe_customerID' );
if(isset($_POST['submit_vehicle'])){
	$manufacturer = $_POST['usermanufacturer'];
	$model = $_POST['usermodel'];
	$fuel_type = $_POST['fuelselect'];
	$variant = $_POST['uservariant'];
	$stage = $_POST['stage'];
	$vehicle_registration = $_POST['reg']; 
	$transmission = $_POST['transmission'];
	$mileage = $_POST['mileage']; 
	$vehicle_age = $_POST['yearmanufactured'];
	$tool_used = $_POST['toolused'];
	$dealer_file_str = $_POST['dealer_file'];
	$dealer_file = rtrim($dealer_file_str,',');
	$comments = $_POST['comments'];
	$free_options_arr = $_POST['free_options'];
	$free_options = '';
	if($free_options_arr){
		$free_options =implode(", ",$free_options_arr);
	}
	$paid = $_POST['paid_options'];
	
	$user_mobile = get_user_meta($user_id,'user_mobile');
	$user_mobile = $user_mobile[0];
	
	$otherAmount = 0; 
	if($paid){
		$paid_options =implode(", ",$paid);
		$otherAmount = array_sum($paid);
	}

	if( empty( $manufacturer ) || empty( $model ) || empty( $fuel_type ) || empty( $variant ) || empty( $stage ) || empty( $vehicle_registration ) || empty( $transmission ) || empty( $mileage ) || empty( $vehicle_age ) || empty( $tool_used ) || empty( $dealer_file ) ){
		echo "<h2 style='color:red;'>Please enter all required fields</h2>";
	}else{
		$secret_key = get_option( 'dealer_secret_key' );
		$stripe_key = $secret_key;
		
		//delete_user_meta($user_id, '_stripe_customerID' );
		$user_id = get_current_user_id();
		$address = get_user_meta($user_id, 'user_address', true);
		$CustomerID = get_user_meta($user_id, '_stripe_customerID', true);
		$stripeToken = $_POST['stripeToken']; 
		
		$defaultAmount = get_option( 'file_price' );
		$amount = $defaultAmount + $otherAmount ;
		$amount = $amount * 100;
		
		$taxAmount  = $amount/100 * 20;
		$fullTotal = $taxAmount + $amount;
		
		$dateinvoice = date('Y-m-d');
		$invoiceItem = $dateinvoice.' '.$model.' '.$vehicle_registration.' '.$stage;
		
		if( !empty( $CustomerID ) ){	
			$customer_key = $CustomerID;
			require_once(ABSPATH.'/wp-content/plugins/dealer_pro/tocken/payment_charge.php');
			
			if($charge->paid == true){
				echo "<h1>Thanks Successfully send</h1>";
				
				$my_post = array(
				  'post_title'    		 => $model,
				  'post_content' 		 => $variant,
				  'post_type'            => 'file',
				  'post_status'          => 'publish'			  
				 );
				$post_ins_id = wp_insert_post( $my_post );
				// Insert the post into the database
				$date = date('Y-m-d');
				
				$html_pdf = pdfpage();
				$html_pdf = str_replace( '#invoicenumber#' ,$post_ins_id, $html_pdf );
				$html_pdf = str_replace( '#invoicedate#' ,$date, $html_pdf );
				$html_pdf = str_replace( '#invoicedate_order#' ,$date, $html_pdf );
				$html_pdf = str_replace( '#total_amount#' ,$_POST['total_amount'], $html_pdf );
				$html_pdf = str_replace( '#tax_amount#' ,$taxAmount, $html_pdf );
				$html_pdf = str_replace( '#total_invoice_amount#' ,$fullTotal, $html_pdf );
				$html_pdf = str_replace( '#addressinvoice#' ,$address, $html_pdf );
				$html_pdf = str_replace( '#invoice_item#' ,$invoiceItem, $html_pdf );

				require_once( dirname(__FILE__) . '/dompdf/dompdf_config.inc.php');

				$dompdf = new DOMPDF();
				$dompdf->load_html($html_pdf);
				//$dompdf->set_paper("A4", "Portrait");
				$dompdf->set_paper("A4", "Portrait");
				$dompdf->render();
						
				$pdf_gen = $dompdf->output();
					 
				$now = new DateTime();
				$now->format('Y-m-d H:i:s');    // MySQL datetime format
				$datetime = $now->getTimestamp();
				$upload_dir = wp_upload_dir();
				$user_dirname = $upload_dir['basedir'].'/invoices';
				if ( ! file_exists( $user_dirname ) ) {
					wp_mkdir_p( $user_dirname );
				}
				$output_file = fopen( $user_dirname.'/'.'dealer_invice'.$post_ins_id.'.pdf' , 'wb' );

				$downloadPDF = home_url()."/wp-content/uploads/invoices/dealer_invice".$post_ins_id.".pdf";

				//write the pdf the into the output file
				fwrite($output_file, $pdf_gen);

				fclose($output_file);
						
				update_post_meta( $post_ins_id, 'fuel_type', $fuel_type );
				update_post_meta( $post_ins_id, 'manufacturer', $manufacturer );
				update_post_meta( $post_ins_id, 'model', $model );
				update_post_meta( $post_ins_id, 'variant', $variant );
				update_post_meta( $post_ins_id, 'stage', $stage );
				update_post_meta( $post_ins_id, 'vehicle_registration', $vehicle_registration );
				update_post_meta( $post_ins_id, 'transmission', $transmission );
				update_post_meta( $post_ins_id, 'mileage', $mileage );
				update_post_meta( $post_ins_id, 'tool_used', $tool_used );
				update_post_meta( $post_ins_id, 'vehicle_age', $vehicle_age );
				update_post_meta( $post_ins_id, 'free_options', $free_options);
				update_post_meta( $post_ins_id, 'paid_options', $paid_options );
				update_post_meta( $post_ins_id, 'dealer_file', $dealer_file );
				update_post_meta( $post_ins_id, 'invoice_link', $downloadPDF );
				update_post_meta( $post_ins_id, 'invoice_price', $fullTotal );
				update_post_meta( $post_ins_id, 'comments', $comments );
				
				$user_info = get_userdata($user_id);
				$user_email = $user_info->user_email;
				$admin_email = get_option('admin_email');
				
				$html_temp = mail_template_file_rec();
				add_filter( 'wp_mail_content_type','wp_set_content_type' );
				wp_mail( $user_email, 'File Received', $html_temp );
				wp_mail( $admin_email, 'File Received', $html_temp);
				remove_filter( 'wp_mail_content_type', 'wp_set_content_type' );
				$user_mobile = '+447377126544';
				send_message($user_mobile);
				//Generate PDF
			}
		}elseif ( empty( $stripeToken ) ) {
			 echo "<h1>Credit Card required</h1>";
		}elseif(!empty($stripeToken) ){
			$stripe_api_key = get_option( 'dealer_stripe_key' );
			
			$provider_user_info = get_userdata($user_id);
			
			$tocken =  $stripeToken;
			$email = $provider_user_info->data->user_email;
			
			require_once(ABSPATH.'/wp-content/plugins/dealer_pro/tocken/register_stripe.php'); 
			$customer_id = $customer->id; 
			
			$stripe_user_name = $customer->sources->data[0]->name;
			update_user_meta($user_id, '_stripe_customerID', $customer_id);
			
			$customer_key = $customer_id;
			require_once(ABSPATH.'/wp-content/plugins/dealer_pro/tocken/payment_charge.php');
			
			if($charge->paid == true){
				echo "<h1>Successfully send</h1>";
				$my_post = array(
				  'post_title'    		 => $model,
				  'post_content' 		 => $variant,
				  'post_type'            => 'file',
				  'post_status'          => 'publish'			  
				 );
				$post_ins_id = wp_insert_post( $my_post );
				
				$date = date('Y-m-d');
				
				$html_pdf = pdfpage();
				$html_pdf = str_replace( '#invoicenumber#' ,$post_ins_id, $html_pdf );
				$html_pdf = str_replace( '#invoicedate#' ,$date, $html_pdf );
				$html_pdf = str_replace( '#invoicedate_order#' ,$date, $html_pdf );
				$html_pdf = str_replace( '#total_amount#' ,$_POST['total_amount'], $html_pdf );
				$html_pdf = str_replace( '#tax_amount#' ,$taxAmount, $html_pdf );
				$html_pdf = str_replace( '#total_invoice_amount#' ,$fullTotal, $html_pdf );
				$html_pdf = str_replace( '#addressinvoice#' ,$address, $html_pdf );
				$html_pdf = str_replace( '#invoice_item#' ,$invoiceItem, $html_pdf );

				require_once( dirname(__FILE__) . '/dompdf/dompdf_config.inc.php');

				$dompdf = new DOMPDF();
				$dompdf->load_html($html_pdf);
				//$dompdf->set_paper("A4", "Portrait");
				$dompdf->set_paper("A4", "Portrait");
				$dompdf->render();
						
				$pdf_gen = $dompdf->output();
					 
				$now = new DateTime();
				$now->format('Y-m-d H:i:s');    // MySQL datetime format
				$datetime = $now->getTimestamp();
				$upload_dir = wp_upload_dir();
				$user_dirname = $upload_dir['basedir'].'/invoices';
				if ( ! file_exists( $user_dirname ) ) {
					wp_mkdir_p( $user_dirname );
				}
				$output_file = fopen( $user_dirname.'/'.'dealer_invice'.$post_ins_id.'.pdf' , 'wb' );

				$downloadPDF = home_url()."/wp-content/uploads/invoices/dealer_invice".$post_ins_id.".pdf";

				//write the pdf the into the output file
				fwrite($output_file, $pdf_gen);

				fclose($output_file);
				
				//update_post_meta( $post_ins_id, '_pdf_download', $downloadPDF );
				
				update_post_meta( $post_ins_id, 'fuel_type', $fuel_type );
				update_post_meta( $post_ins_id, 'manufacturer', $manufacturer );
				update_post_meta( $post_ins_id, 'model', $model );
				update_post_meta( $post_ins_id, 'variant', $variant );
				update_post_meta( $post_ins_id, 'stage', $stage );
				update_post_meta( $post_ins_id, 'vehicle_registration', $vehicle_registration );
				update_post_meta( $post_ins_id, 'transmission', $transmission );
				update_post_meta( $post_ins_id, 'mileage', $mileage );
				update_post_meta( $post_ins_id, 'tool_used', $tool_used );
				update_post_meta( $post_ins_id, 'vehicle_age', $vehicle_age );
				update_post_meta( $post_ins_id, 'free_options', $free_options);
				update_post_meta( $post_ins_id, 'paid_options', $paid_options );
				update_post_meta( $post_ins_id, 'invoice_link', $downloadPDF );
				update_post_meta( $post_ins_id, 'invoice_price', $fullTotal );
				update_post_meta( $post_ins_id, 'dealer_file', $dealer_file );
				update_post_meta( $post_ins_id, 'comments', $comments );
			
				$user_info = get_userdata($user_id);
				$user_email = $user_info->user_email;
				$admin_email = get_option('admin_email');
				$user_mobile = get_user_meta($user_id,'user_mobile');
				$user_mobile = $user_mobile[0];
				$html_temp = mail_template_file_rec();
				add_filter( 'wp_mail_content_type','wp_set_content_type' );
				wp_mail( $user_email, 'File Received', $html_temp );
				wp_mail( $admin_email, 'File Received', $html_temp);
				remove_filter( 'wp_mail_content_type', 'wp_set_content_type' );
				$user_mobile = '+447377126544';
				send_message($user_mobile);
				//Generate PDF
			}
		}
	}
}
	?>
<div class="container11">
  <ul class="nav nav-tabs">
	<li><a data-toggle="tab" href="#upload_files">UPLOADED</a></li>
    <li class="active"><a data-toggle="tab" href="#submit_files">Submit New</a></li>
    <li><a data-toggle="tab" href="#invoices">INVOICES</a></li>
	<h3><a href="<?php echo wp_logout_url(); ?>" class="btn btn-default pull-right margin-clear">Logout</a></h3>
  </ul>

  <div class="tab-content">
    <div id="submit_files" class="tab-pane fade in active">
        <div class="tab-content" id="submit_files">
          <form action="" id="mem_form" class="form-horizontal standardajax standardajax-validate-only elite-review-form js-portalform" enctype="multipart/form-data" method="post">
            <!--<input type="hidden" name="userdata" value="true">
            <input type="hidden" name="method" value="upload">
            <input type="hidden" name="manufacturer">
            <input type="hidden" name="model">
            <input type="hidden" name="variant"> 
            <div class="gotcha">Please do not fill this: <input type="text" name="gotcha"></div>-->
            <div class="row">
              <h4 class="col-sm-9 col-sm-offset-3 mt-30">Select vehicle:</h4>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label">Manufacturer<span class="requiredinput">:*</span></label>
              <div class="col-sm-9">
                <input name="usermanufacturer" value="<?php if(isset($_POST['usermanufacturer'])){ echo $_POST['usermanufacturer']; } ?>" required type="text" class="form-control formrequired">
              </div>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label">Model<span class="requiredinput">:*</span></label>
              <div class="col-sm-9">
                <input name="usermodel" value="<?php if(isset($_POST['usermodel'])){ echo $_POST['usermodel']; }  ?>"  required type="text" class="form-control formrequired">
              </div>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label">Fuel type<span class="requiredinput">:*</span></label>
              <div class="col-sm-9">
                <select name="fuelselect" required class="form-control formrequired">
                  <option value="">Select fuel type</option>
                  <option value="Diesel" <?php if(  isset($_POST['fuelselect']) && $_POST['fuelselect'] == 'Diesel' ){ echo 'selected'; } ?> >Diesel</option>
                  <option value="Petrol" <?php if(  isset($_POST['fuelselect']) && $_POST['fuelselect'] == 'Petrol' ){ echo 'selected'; } ?>>Petrol</option>
                </select>
              </div>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label">Variant<span class="requiredinput">:*</span></label>
              <div class="col-sm-9">
                <input name="uservariant" value="<?php if(isset($_POST['uservariant'])){ echo $_POST['uservariant']; } ?>"  required type="text" class="form-control formrequired">
              </div>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label">Stage<span class="requiredinput">:*</span></label>
              <div class="col-sm-9">
                <select name="stage" required class="form-control formrequired">
                  <option value="">Select your stage</option>
                  <option value="No tuning" <?php if(  isset($_POST['stage']) && $_POST['stage'] == 'No tuning' ){ echo 'selected'; } ?> >No tuning</option>
                  <option value="Economy" <?php if(  isset($_POST['stage']) && $_POST['stage'] == 'Economy' ){ echo 'selected'; } ?> >Economy</option>
                  <option value="Stage 1" <?php if(  isset($_POST['stage']) && $_POST['stage'] == 'Stage 1' ){ echo 'selected'; } ?> >Stage 1</option>
                  <option value="Stage 2" <?php if(  isset($_POST['stage']) && $_POST['stage'] == 'Stage 2' ){ echo 'selected'; } ?> >Stage 2</option>
                  <option value="Stage 3" <?php if( isset($_POST['stage']) && $_POST['stage'] == 'Stage 3' ){ echo 'selected'; } ?> >Stage 3</option>
                  <option value="Stage 4" <?php if(  isset($_POST['stage']) && $_POST['stage'] == 'Stage 3' ){ echo 'selected'; } ?> >Stage 4</option>
                </select>
              </div>
            </div>
            <div class="row mb-30">
              <p class="col-sm-9 col-sm-offset-3"></p>
            </div>
            <div class="row">
              <h4 class="col-sm-9 col-sm-offset-3">More details:</h4>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label" for="reg">Vehicle registration<span class="requiredinput">:*</span></label>
              <div class="col-sm-9">
                <input id="reg" name="reg" value="<?php if(isset($_POST['reg'])){ echo $_POST['reg']; } ?>" required type="text" placeholder="EG. YE61 DNK" class="form-control formrequired placeholder">
              </div>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label" for="name">Transmission<span class="requiredinput">:*</span></label>
              <div class="col-sm-9">
                <select name="transmission" required class="form-control formrequired">
                  <option value="">Select:</option>
                  <option value="Manual 5 speed" <?php if( isset($_POST['transmission']) && $_POST['transmission'] == 'Manual 5 speed' ){ echo 'selected'; } ?>  >Manual 5 speed</option>
                  <option value="Manual 6 speed" <?php if( isset($_POST['transmission']) &&  $_POST['transmission'] == 'Manual 6 speed' ){ echo 'selected'; } ?> >Manual 6 speed</option>
                  <option value="Automatic" <?php if( isset($_POST['transmission']) &&  $_POST['transmission'] == 'Automatic' ){ echo 'selected'; } ?>>Automatic</option>
                  <option value="DSG" <?php if( isset($_POST['transmission']) &&  $_POST['transmission'] == 'DSG' ){ echo 'selected'; } ?>>DSG</option>
                  <option value="DSG 6" <?php if( isset($_POST['transmission']) &&  $_POST['transmission'] == 'DSG 6' ){ echo 'selected'; } ?>>DSG 6</option>
                  <option value="DSG 7" <?php if( isset($_POST['transmission']) &&  $_POST['transmission'] == 'DSG 7' ){ echo 'selected'; } ?>>DSG 7</option>
                  <option value="DKG" <?php if( isset($_POST['transmission']) &&  $_POST['transmission'] == 'DKG' ){ echo 'selected'; } ?>>DKG</option>
                  <option value="DCT" <?php if( isset($_POST['transmission']) &&  $_POST['transmission'] == 'DCT' ){ echo 'selected'; } ?>>DCT</option>
                  <option value="SMG" <?php if( isset($_POST['transmission']) &&  $_POST['transmission'] == 'SMG' ){ echo 'selected'; } ?>>SMG</option>
                  <option value="SMG 2" <?php if( isset($_POST['transmission']) &&  $_POST['transmission'] == 'SMG 2' ){ echo 'selected'; } ?>>SMG 2</option>
                  <option value="Tiptronic" <?php if( isset($_POST['transmission']) &&  $_POST['transmission'] == 'Tiptronic' ){ echo 'selected'; } ?>>Tiptronic</option>
                  <option value="Multitronic" <?php if( isset($_POST['transmission']) &&  $_POST['transmission'] == 'Multitronic' ){ echo 'selected'; } ?>>Multitronic</option>
                </select>
              </div>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label" for="name">Mileage<span class="requiredinput">:*</span></label>
              <div class="col-sm-9">
<input id="mileage" required name="mileage" value="<?php if(isset($_POST['mileage'])){ echo $_POST['mileage']; } ?>" type="text" placeholder="Please enter numbers only" data-regex="[0-9]+" class="form-control formrequired placeholder">
              </div>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label" for="name">Vehicle age<span class="requiredinput">:*</span></label>
              <div class="col-sm-9">
                <select name="yearmanufactured" required class="form-control formrequired">
					<option value="">Select:</option>
					<?php
					$yearend = date('Y');
					$startyear = 1995 ; 
					for( $startyear ; $startyear  <= $yearend ; $startyear ++ ){ ?>
						<option value="<?php echo $startyear; ?>" <?php if(  isset($_POST['yearmanufactured']) && $startyear == $_POST['yearmanufactured'] ){ echo 'selected'; } ?> ><?php echo $startyear; ?></option><?php
					}
					?>
                </select>
              </div>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label" for="location">Tool used<span class="requiredinput">:*</span></label>
              <div class="col-sm-9">
                <input id="toolused" name="toolused" value="<?php if(isset($_POST['toolused'])){ echo $_POST['toolused']; } ?>" required type="text" placeholder="The tool you used to read the vehicle" class="form-control formrequired placeholder">
              </div>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label" for="location">Free options:</label>
              <div class="col-sm-9">
                <div class="well">
                  <div class="row">
                    <div class="col-xs-6"><label>
                      <input type="checkbox"  value="DPF Disable" name="free_options[]"> DPF Disable
                      </label><br><label>
                      <input type="checkbox" value="EGR Disable" name="free_options[]"> EGR Disable
                      </label><br><label>
                      <input type="checkbox" value="Speed Limiter Disable" name="free_options[]"> Speed Limiter Disable
                      </label><br><label>
                      <input type="checkbox" value="RPM Limiter Increase" name="free_options[]"> RPM Limiter Increase
                      </label><br>
                    </div>
                    <div class="col-xs-6"><label>
                      <input type="checkbox" value="Lambda/O2 Disable" name="free_options[]"> Lambda/O2 Disable
                      </label><br><label>
                      <input type="checkbox" value="Intake Manifold Flap Disable" name="free_options[]"> Intake Manifold Flap Disable
                      </label><br><label>
                      <input type="checkbox" value="AdBlue (SCR) Disable" name="free_options[]"> AdBlue (SCR) Disable
                      </label><br><label>
                      <input type="checkbox" value="Specific DTC Disable" name="free_options[]"> Specific DTC Disable
                      </label><br>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label" for="location">Paid options:</label>
              <div class="col-sm-9">
                <div class="well">
                  <div class="row">
                    <div class="col-xs-12"><label>
                      <input class="paid_options" type="checkbox" value="25" name="paid_options[]"> Exhaust Flap Control <span class="label label-warning">+£25
                      </span></label><br><label>
                      <input class="paid_options"  type="checkbox" value="25" name="paid_options[]"> Launch Control <span class="label label-warning">+£25
                      </span></label><br><label>
                      <input class="paid_options" type="checkbox" value="25" name="paid_options[]"> No-Lift Shift <span class="label label-warning">+£25
                      </span></label><br><label>
                      <input class="paid_options"  type="checkbox" value="20" name="paid_options[]"> Pops &amp; Bangs <span class="label label-warning">+£20
                      </span></label><br><label>
                      <input class="paid_options"  type="checkbox" value="15" name="paid_options[]"> Hard Rev Cut <span class="label label-warning">+£15
                      </span></label><br><label>
                      <input class="paid_options"  type="checkbox" value="10" name="paid_options[]"> Start-Stop Deactivation <span class="label label-warning">+£10
                      </span></label><br><label>
                      <input class="paid_options"  type="checkbox" value="10" name="paid_options[]"> Tuning On Sport Button <span class="label label-warning">+£10
                      </span></label><br><label>
                      <input class="paid_options"  type="checkbox" value="10" name="paid_options[]"> Cylinder On Demend <span class="label label-warning">+£10
                      </span></label><br><label>
                      <input class="paid_options"  type="checkbox" value="15" name="paid_options[]"> Sport Displays Calibration Module <span class="label label-warning">+£15
                      </span></label><br>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label" for="location">Comments:</label>
              <div class="col-sm-9">
                <textarea class="form-control" name="comments"><?php if(isset($_POST['comments'])){ echo $_POST['comments']; } ?></textarea>
              </div>
            </div>
            <div class="row mb-20">
              <label class="col-sm-3 control-label" for="name">File to remap<span class="requiredinput">:*</span></label>
              <div class="col-sm-9" data-toggle="popover" data-html="true" data-content="<strong>Allowed files:</strong> bin, dat, fpf, dec, unq, cod, mpc, fls, ori, org, mod, zip, rar, tun, pak" data-trigger="hover" data-placement="top" data-original-title="" title="">
			<?php

$args = array(
    "unique_identifier" => "my_subscription_form_file_upload",
    "allowed_extensions" => "zip,pdf,rar",
	"on_success_set_input_value" => "#file_to_remap",
    "on_success_alert" => "Your file was uploaded. Please continue with your subscription operation."
);
echo ajax_file_upload( $args );
// echo do_shortcode('[ajax-file-upload]'); ?>

			<div class="click_here">click here to upload !</div>
			<textarea class="form-control hide" name="dealer_file" value="" id="file_to_remap" required ></textarea>
              </div>
				<input type="hidden" name="default_amount" value="<?php echo get_option( 'file_price' ); ?>" id="default_amount">
				<input type="hidden" name="additional_amount" value="0" id="additional_amount">
				<input type="hidden" name="total_amount" value="<?php echo get_option( 'file_price' ); ?>" id="dealer_totPrice">

            </div>
			<?php
			$CustomerID = get_user_meta($user_id, '_stripe_customerID', true);
			if( empty( $CustomerID ) ){
			?>
			<div class="row mb-20">
				<label class="col-sm-3 control-label" for="name">Add Card Info<span class="requiredinput">:*</label>
				<div class="col-sm-9" >
					<p style="color:green;display:none;font-weight:bold;font-size:18px;" id="msgcreditsaved">Your Credit Card has been saved successfully</p>
					<input type="button" onclick="saveinfo();" id="credit_cardinfo22" class="btn btn-primary" value="Add Credit Card" />
				</div>
            </div>
			<?php
			}
			?>
			<script src="https://checkout.stripe.com/checkout.js"></script>
			<script>
			function sum(input){
						 
			 if (toString.call(input) !== "[object Array]")
				return false;
				  
						var total =  0;
						for(var i=0;i<input.length;i++)
						  {                  
							if(isNaN(input[i])){
							continue;
							 }
							  total += Number(input[i]);
						   }
						 return total;
						}
			jQuery('.paid_options').on('change', function() {
				  var favorite = [];
				  var total=0;
					   jQuery.each(jQuery("input[name='paid_options[]']:checked"), function(){ 
							favorite.push(jQuery(this).val());
						});
					   total = sum(favorite);
					   var additional_amount = jQuery('#additional_amount').val(total);
						var default_amount =  jQuery('#default_amount').val();
						var additional_amount = jQuery('#additional_amount').val();
						var total_amount = parseInt(default_amount)  + parseInt(additional_amount);
						jQuery('#dealer_totPrice').val(total_amount);
						console.log(total_amount);
			});
				var handler = StripeCheckout.configure
				({
					key: '<?php echo get_option( 'dealer_stripe_key' ); ?>',
					image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
					token: function(token) 
					{
						jQuery('#mem_form').append("<input type='hidden' name='stripeToken' value='" + token.id + "' />"); 
						jQuery('#credit_cardinfo22').hide(); 
						jQuery('#msgcreditsaved').show(); 
						setTimeout(function(){
							//jQuery('#mem_form').submit(); 
							
						}, 200); 
					}
				});
				jQuery(".checkbox").change(function() {
					if(this.checked) {
						//Do stuff
					}
				});
				function saveinfo(){
			
					//var amount_feild = jQuery('#dealer_totPrice').val(total_amount);
					var amount = jQuery('#dealer_totPrice').val();
					
					total=amount*100;
					//total = 100;
					handler.open({
					//name: "Membership Fee",
					name: "Add Credit Card",
					//panelLabel: "Save",
					currency : 'USD',
					description: 'Charges( '+amount+' USD )',
					//description: 'Save card information',
					amount: total
					});
				}
			</script>
            <p class="text-right">
              <input name="submit_vehicle" type="submit" class="btn btn-default" value="Submit File">
            </p>
          </form>
        </div>
    </div>
<div id="invoices" class="tab-pane fade">
    <?php
global $current_user;                     

$args = array(
  'author'        =>  $current_user->ID,
  'post_type' => 'file',  
  'orderby'       =>  'post_date',
  'order'         =>  'DESC',
  'posts_per_page' => -1 // no limit
);


$current_user_posts = get_posts( $args );
?>
<div class="container">
<div class="row">
<?php
if(!empty($current_user_posts)){
foreach($current_user_posts as $post){
	$invoice_link = get_post_meta( $post->ID, 'invoice_link', true );
	$invoice_number = get_post_meta( $post->ID, 'invoice_number', true );
	$invoice_price = get_post_meta( $post->ID, 'invoice_price', true );
	 echo '<div class="col-md-12"><div class="btn btn-default d-right margin-clear invoice_link">Invoice # '.$post->ID.' - &#163;'.$invoice_price.' INC VAT</div></div>';
	 echo '<div class="col-md-12"><a href="'.$invoice_link.'" class="invoice_link" download>Download PDF</a></div>';
}

?>
</div>
<?php 
}else{
	echo 'No invoice yet';
}
?>
</div>
</div>
    <div id="upload_files" class="tab-pane fade">
<?php
global $current_user;                     

$args = array(
  'author'        =>  $current_user->ID,
  'post_type' => 'file',  
  'orderby'       =>  'post_date',
  'order'         =>  'DESC',
  'posts_per_page' => -1 // no limit
);


$current_user_posts = get_posts( $args );
if(!empty($current_user_posts)){
?>
<div class="container22"><div class="row ">
<?php
foreach($current_user_posts as $post){
	$manufacturer = get_post_meta( $post->ID, 'manufacturer', true );
	$model = get_post_meta( $post->ID, 'model', true );
	$fuel_type = get_post_meta( $post->ID, 'fuel_type', true );
	$variant = get_post_meta( $post->ID, 'variant', true );
	$stage = get_post_meta( $post->ID, 'stage', true );
	$vehicle_registration = get_post_meta( $post->ID, 'vehicle_registration', true );
	$transmission = get_post_meta( $post->ID, 'transmission', true );
	$mileage = get_post_meta( $post->ID, 'mileage', true );
	$tool_used = get_post_meta( $post->ID, 'tool_used', true );
	$vehicle_age = get_post_meta( $post->ID, 'vehicle_age', true );
	$free_options = get_post_meta( $post->ID, 'free_options', true );
	$paid_options = get_post_meta( $post->ID, 'paid_options', true );
	$dealer_file = get_post_meta( $post->ID, 'dealer_file', true );
	$void_reason = get_post_meta( $post->ID, 'void_reason', true );
	$comments = get_post_meta( $post->ID, 'comments', true );
	$currentdate = get_the_date( 'l F j, Y' );

	echo ' <div class="main_container">
	<div id="show_date">'.$currentdate.'</div> 
	<div class="show_data"><div class="manufacturer">'.$manufacturer.' >> '.$model.' >> '.$vehicle_age.' >> '.$transmission.' </div><div class="stage_row"><div>'.$stage.'</div></div>';
	
	if($dealer_file){
	echo '<a href="'.$dealer_file.'" class="download_file btn btn-submit" download>Download File</a>';
	}else{
	echo '<a href="#" class="void_link btn btn-default d-right margin-clear" download>Void File</a><p class="void_reason"><span>Void Reason: </sapn>'.$void_reason.'</p>';
	}
	echo '</div></div>';
}
}else{
	echo 'No uploaded yet';
}
?>
    </div>
  </div>
</div>
<?php
	} else {
		?>
   <section class="page-right-sidebar page-section-ptb">
	   <!--<div class="objects-left"><img class="img-responsive objects-1" src="images/objects/03.jpg" alt=""></div>-->
	   <!--<div class="objects-right"><img class="img-responsive objects-2" src="images/objects/04.jpg" alt=""></div>-->
	   <div class="container11">
		  <div class="row">
			 <div class="col-md-8">
				<ul class="nav nav-tabs">
				   <li class=""><a href="#tab-login" data-toggle="tab" aria-expanded="false">LOG IN</a></li>
				   <li class="active"><a href="#tab-register" data-toggle="tab" aria-expanded="true">REGISTER</a></li>
				</ul>          

				<div class="tab-content">
				   <div class="tab-pane fade" id="tab-login">
						<form action="" id="login_form32" class=""  method="post">
							<div class="form-group">
								<label  class="col-sm-5 control-label">User Name<span class="requiredinput">:*</span></label>
								<div class="col-sm-7">
									<input type="text" required class="form-control formrequired"  name="user_loginname" value="<?php echo $_POST['user_loginname']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label  class="col-sm-5 control-label">Password<span class="requiredinput">:*</span></label>
								<div class="col-sm-7">
								<input type="password" required class="form-control formrequired"  name="user_paswd" value="<?php echo $_POST['user_paswd']; ?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-5 col-sm-9">
								   <input name="sign_inuser" type="submit" id="submit_btn_dealer" class="btn btn-default" value="SIGN IN">
								   <p><a href="#!" onclick="forgotForm();">Forgot Password</a></p>
								</div>
							 </div>
							 <script>
								function forgotForm(){
									jQuery("#forgot32").show();
									jQuery("#login_form32").hide();
								}
							</script>
						</form>
						<form action="" id="forgot32" style="display:none;"  method="post">
							<div class="form-group">
								<label  class="col-sm-5 control-label">Email<span class="requiredinput">:*</span></label>
								<div class="col-sm-7">
									<input type="email" required class="form-control formrequired"  name="user_loginname" value="<?php echo $_POST['user_loginname']; ?>">
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-offset-5 col-sm-9">
								   <input name="sign_forgot" type="submit" id="submit_btn_dealer" class="btn btn-default" value="Forgot">
								   <p><a href="#!" onclick="signinForm();">Sign In</a></p>
								</div>
							 </div>
							 <script>
								function signinForm(){
									jQuery("#forgot32").hide();
									jQuery("#login_form32").show();
								}
							</script>
						</form>
						</div>
				   <div class="tab-pane fade active in" id="tab-register">
					  <form action="" class="form-horizontal mt-30"  method="post">
						 <input type="hidden" name="method" value="register">
						 <div class="form-group">
							<label  class="col-sm-5 control-label">Name<span class="requiredinput">:*</span></label>
							<div class="col-sm-7">
							   <input type="text" required class="form-control formrequired" id="name32" name="user_name" value="<?php echo $_POST['user_name']; ?>">
							   <div  style="display:none;color:red;font-size:18px;" id="name_32">Name is required</div>
							</div>
						 </div>
						 <div class="form-group">
							<label for="inputEmail2" class="col-sm-5 control-label">Company name<span class="requiredinput">:*</span></label>
							<div class="col-sm-7">
							   <input type="text" class="form-control formrequired" id="company32" required name="company" value="<?php echo $_POST['company']; ?>">
								<div  style="display:none;color:red;font-size:18px;" id="company_32">Company is required</div>
							</div>
						 </div>
						 <div class="form-group">
							<label  class="col-sm-5 control-label">Email<span class="requiredinput">:*</span></label>
							<div class="col-sm-7">
							   <input type="email" required class="form-control formrequired" id="email32" name="email" value="<?php echo $_POST['email']; ?>">
							   <div  style="display:none;color:red;font-size:18px;" id="email_32">Email is required</div>
							</div>
						 </div>
						 <div class="form-group">
							<label for="inputEmail4" class="col-sm-5 control-label">Mobile number * (UK only):</label>
							<div class="col-sm-7">
							   <input type="text" required class="form-control" name="mobile" value="<?php echo $_POST['mobile']; ?>" >
							   <div  style="display:none;color:red;font-size:18px;" id="mobile_number">Mobile number is required</div>
							</div>
						 </div>
						 <div class="form-group">
							<label  class="col-sm-5 control-label">Billing address<span class="requiredinput">:*</span></label>
							<div class="col-sm-7">
							   <textarea required class="form-control"  name="address" rows="4"><?php echo $_POST['address']; ?></textarea>
							   
							   <div  style="display:none;color:red;font-size:18px;" id="billing_address">Billing address is required</div>
							</div>
						 </div>
						 <div class="form-group">
							<label  class="col-sm-5 control-label">VAT number:</label>
							<div class="col-sm-7">
							   <input type="text" class="form-control" id="valnumber32" name="vatnumber"  value="<?php echo $_POST['vatnumber']; ?>" >
							</div>
						 </div>
						 <div class="form-group">
							<label for="inputPassword1" class="col-sm-5 control-label">Password<span class="requiredinput">:*</span></label>
							<div class="col-sm-7">
							   <input type="password" required value="<?php echo $_POST['password']; ?>" class="form-control" id="chk_pass" name="password">
							</div>
						 </div>
						 <div class="form-group">
							<label  class="col-sm-5 control-label">Retype password<span class="requiredinput">:*</span></label>
							<div class="col-sm-7">
							   <input type="password" required class="form-control" id="confirm_password" value="<?php echo $_POST['password2']; ?>" name="password2">
							   <div  style="display:none;color:red;font-size:18px;" id="showmsg_psd">Password does not match</div>
							</div>
						 </div>
						 <div class="form-group">
							<div class="col-sm-offset-5 col-sm-9">
							   <input name="submit_user" type="submit" id="submit_btn_dealer" class="btn btn-default" value="REGISTER">
							</div>
						 </div>
					  </form>
				   </div>
				</div>
			 </div>
			 <script>
				jQuery( document ).ready(function() {
					//name front end handling
					/* jQuery("#name32").focusout(function(){
						var name = jQuery("#name32").val().length;
						if ( name < 1  ) {
							jQuery("#submit_btn_dealer").attr("disabled","disabled");
							jQuery("#name_32").show();
							return false;
						}else{
							jQuery("#submit_btn_dealer").attr("disabled",false);
							jQuery("#name_32").hide();
						}
						
					});
					jQuery("#company32").focusout(function(){
						var name = jQuery("#company32").val().length;
						if ( name < 1  ) {
							jQuery("#submit_btn_dealer").attr("disabled","disabled");
							jQuery("#mobile_number").show();
							return false;
						}else{
							jQuery("#submit_btn_dealer").attr("disabled",false);
							jQuery("#mobile_number").hide();
						}
						
					}); */
					jQuery("#confirm_password").focusout(function(){
						var password = jQuery("#chk_pass").val();
						var confirmPassword = jQuery("#confirm_password").val();
						if (password != confirmPassword) {
							jQuery("#submit_btn_dealer").attr("disabled","disabled");
							jQuery("#showmsg_psd").show();
							return false;
						}
						if (password == confirmPassword) {
							jQuery("#submit_btn_dealer").attr("disabled",false);
							jQuery("#showmsg_psd").hide();
						}
					});
				});
			 </script>
			 <div class="col-md-4 sky-sidebar">
				<div id="search-form">
				</div>
				<div class="well mb-30">
				   <div class="feature-box text-center">
					  <div class="icon">
						 <i class="glyph-icon flaticon-ribbon"></i>
					  </div>
					  <div class="content">
						 <h6><a href="<?php echo home_url(); ?>/become-a-dealer">Dealer Opportunities</a></h6>
						 <p>Become a dealer and benefit from our years of experience in the industry</p>
					  </div>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</section>
	<?php
	}
	$html = ob_get_clean();
	return $html;
}
function geo_my_wp_script_back_css() {

}

function geo_my_wp_script_front_css() {
		/* CSS */
        wp_register_style('geo_my_wp_style_bootstrap', plugins_url('css/bootstrap.min.css',__FILE__));
        wp_register_style('geo_my_wp_style', plugins_url('css/dealer_pro.css',__FILE__));
        wp_enqueue_style('geo_my_wp_style');
        wp_enqueue_style('geo_my_wp_style_bootstrap');
}

		add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
		add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );
function my_ajax_rt() {
	
}

function geo_my_wp_script_back_js() {
	
}



function geo_my_wp_script_front_js() {
		wp_register_script('dealer_pro_js', plugins_url('js/dealer_pro.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('dealer_pro_js');
		wp_register_script('bootstrap_wp_script', plugins_url('js/bootstrap.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('bootstrap_wp_script');

}
