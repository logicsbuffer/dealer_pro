<?php
require_once('Stripe.php'); 

// live 5star stripe id
Stripe::setApiKey($stripe_key);
$charge = Stripe_Charge::create(array(
	"amount" => round($amount), 
	"currency" => "usd",
	"customer" => $customer_key)
);
return $charge;