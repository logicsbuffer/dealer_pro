<?php
require_once('Stripe.php'); 
  
// MUST be done before any Stripe:: call
Stripe::setApiKey($stripe_key);
$create_account = Stripe_Token::create(array(
    "bank_account" => array(
    "country" => "US",
    "currency" => "usd",
    "account_holder_name" => "Zahid Aslam",
    "account_holder_type" => "individual",
    "routing_number" => "031100102",
    "account_number" => "210437737"
  )
));