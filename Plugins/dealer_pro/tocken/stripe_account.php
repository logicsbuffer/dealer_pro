<?php
require_once('Stripe.php'); 
  
// MUST be done before any Stripe:: call
Stripe::setApiKey($stripe_key);
$create_account = Stripe_Token::create(array(
    "bank_account" => array(
    "country" => "US",
    "currency" => "usd",
    "account_holder_name" => $first_name.' '.$last_name,
    "account_holder_type" => $bnk_acc_type,
    "routing_number" => $bnk_routing_no,
    "account_number" => $bnk_acc_no
  )
));