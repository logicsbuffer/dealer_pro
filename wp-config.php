<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wplogics_wp2');

/** MySQL database username */
define('DB_USER', 'wplogics_wp2');

/** MySQL database password */
define('DB_PASSWORD', 'Q[WvAxs93@GdOj6oa8^28]#2');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9apDZiyzZPiAiRLGyQnV3jTt6q9x2l6mTaI7QlJ0Vwuqy7dLw8SS5l2lUL0bxWzL');
define('SECURE_AUTH_KEY',  'nbiBmj726vvGzYjqCvDWF7Dbgc9j5mj1BCTd4k7OttLNDvDOWGddXIKQYYouspPl');
define('LOGGED_IN_KEY',    '1bVUBDmrckLmbWSiVx2AFEPeeu1EkDqposdwangtTXuInQgXULB8In3CsU1lq4Ka');
define('NONCE_KEY',        'OzkM6FnlP5LfLOjslQmnVQiLo0PBmj5QWQZ6ZYfYiMAOkCJma0J1TezlMXaVZpLe');
define('AUTH_SALT',        '5SbCKAexY56hRA2s2SjoL4bZzr0fisaWKKyaDVh4fdQC49qeWIQ5hpUReQwMzcfo');
define('SECURE_AUTH_SALT', '2bS3Zp0u66y36HdEJp6LsYCz7h4HbaCY268rv57DOFpi8W6t5ZWgDXseODYCPoWX');
define('LOGGED_IN_SALT',   'kzT82TBJ6OHa2vjRByZfN6JxqahOF9cevT6cPGfxVlPtTNtpkpYdwLTXas3S7zgz');
define('NONCE_SALT',       'nOjH3CiZb83VrukbRab1HyMb01Owlc35vaH9CEqNs9VHj1kXf4mpfcxCfERLBHJT');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */



define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', true);
@ini_set('display_errors', E_ALL);



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
